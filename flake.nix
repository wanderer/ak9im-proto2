{
  description = "AK9MI proto 2";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
  };

  outputs = {
    self,
    nixpkgs,
    ...
  }: let
    supportedSystems = ["x86_64-linux" "x86_64-darwin"];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

    courseCode = "ak9im";
    documentName = "proto2";
    pname = courseCode + "_" + documentName;
    version = "0.0.1";

    pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});
    nixpkgsFor = forAllSystems (
      system:
        import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        }
    );
  in rec {
    formatter = forAllSystems (
      system:
        nixpkgsFor.${system}.alejandra
    );

    overlays.default = with pkgs;
      final: prev: {
        watcher = with final;
          writeScriptBin "watch" ''
            out=".latexmkout"
            mkdir "$out"
            latexmk \
              -pvc \
              -outdir="$out" \
              -pdf \
              -pdflatex="pdflatex -synctex=1 -file-line-error -interaction=nonstopmode" \
              -use-make ${documentName}.tex
            rm -r "$out"
          '';

        buildLatex = with final;
        with pkgs;
          stdenv.mkDerivation {
            inherit pname version;

            src = lib.cleanSource ./.;

            nativeBuildInputs = with pkgs; [
              (texlive.combine {
                inherit
                  (texlive)
                  scheme-medium
                  authblk
                  multirow
                  hyperref
                  blindtext
                  etoolbox
                  topiclongtable
                  ;
              })
              gnumake
            ];

            buildPhase = ''
              latexmk \
              -pdf \
              -pdflatex="pdflatex -file-line-error -interaction=nonstopmode" \
              -use-make ${documentName}.tex
            '';
            installPhase = ''
              install -Dm444 -t $out ${documentName}.pdf
            '';
          };
      };

    packages = forAllSystems (system: {
      inherit (nixpkgsFor.${system}) watcher buildLatex;

      default = nixpkgsFor.${system}.watcher;
    });

    defaultPackage =
      forAllSystems (system: self.packages."${system}".buildLatex);

    apps = forAllSystems (system: rec {
      watch = {
        type = "app";
        program = "${self.packages."${system}".watcher}/bin/watch";
      };
      buildLatex = {
        type = "app";
        program = "${self.packages."${system}".buildLatex}";
      };
      # default = buildLatex;
      default = watch;
    });

    devShells = with pkgs;
      forAllSystems (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        };

        nativeBuildInputs = with pkgs; [
          (texlive.combine {
            inherit
              (texlive)
              scheme-medium
              authblk
              multirow
              hyperref
              blindtext
              etoolbox
              topiclongtable
              ;
          })
          gnumake
        ];

        watcher = pkgs.writeShellScriptBin "watcher" ''
          out=".latexmkout"
          mkdir "$out"
          latexmk \
            -pvc \
            -outdir="$out" \
            -pdf \
            -pdflatex="pdflatex -synctex=1 -file-line-error -interaction=nonstopmode" \
            -use-make ${documentName}.tex
          rm -r "$out"
        '';
      in {
        default = nixpkgsFor.${system}.mkShell {
          name = "${pname}-" + version;
          shellHook = ''
            echo " -- in ${pname} dev shell..."
          '';

          nativeBuildInputs = with pkgs; [
            (texlive.combine {
              inherit
                (texlive)
                scheme-full
                multirow
                hyperref
                blindtext
                etoolbox
                topiclongtable
                ;
            })
            gnumake
          ];

          packages = with nixpkgsFor.${system}; [
            watcher

            gnumake
            alejandra
          ];
        };
      });
  };
}
